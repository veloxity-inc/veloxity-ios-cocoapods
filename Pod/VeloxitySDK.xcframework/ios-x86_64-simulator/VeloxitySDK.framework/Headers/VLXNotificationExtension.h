//
//  VLXNotificationExtension.h
//  VeloxitySDK
//
//  Created by Cemal YILMAZ on 30.03.2020.
//  Copyright © 2020 Veloxity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

@interface VLXNotificationExtension : UNNotificationServiceExtension

@end

NS_ASSUME_NONNULL_END
