//
//  vlxAnalytic.h
//  VeloxitySDK
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface vlxAnalytic : NSObject

+ (instancetype) sharedInstance;

- (void) registerLifeCycleCallbacks;

- (void) pageViewStarted:(NSString *)pageName identifier:(NSString *) identifier;
- (void) pageViewStarted:(NSString *)pageName identifier:(NSString *) identifier category:(nullable NSString *) category;

- (void) pageViewEnded:(NSString *)pageName identifier:(NSString *) identifier;
- (void) pageViewEnded:(NSString *)pageName identifier:(NSString *) identifier category:(nullable NSString *) category;

@end
NS_ASSUME_NONNULL_END
