//
//  VeloxitySDK.h
//  VeloxitySDK
//
//  Created by Tolga Tanriverdi on 20/02/16.
//  Copyright © 2016 Veloxity. All rights reserved.
//

#import "Veloxity.h"
#import "VLXNotificationExtension.h"
#import "vlxNetworkTraceTester.h"
#import "vlxPublic.h"
#import "vlxAnalytic.h"
#import <UIKit/UIKit.h>

//! Project version number for VeloxitySDK.
FOUNDATION_EXPORT double VeloxitySDKVersionNumber;

//! Project version string for VeloxitySDK.
FOUNDATION_EXPORT const unsigned char VeloxitySDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <VeloxitySDK/PublicHeader.h>
