//
//  vlxPublic.h
//  veloxitySDK
//
//  Created by Mert Güneş on 17/09/14.
//  Copyright (c) 2014 Mert Güneş. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol vlxPublicDelegate <NSObject>

@optional
/**
 * Delegate function which is called after the network scan is done.
 */
- (void)didFinishScanningNetwork:(NSArray*)resultArray;

/**
 * Delegate function which is called after the ping test is done.
 */
- (void)didFinishPingTest:(NSMutableDictionary*)_sampleDict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the trace route test is done.
 */
- (void)didFinishTraceRoute:(NSMutableDictionary*)_dict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the delay test is done.
 */
- (void)didFinishDelayTest:(NSMutableDictionary*)_sampleDict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the wi-fi speed test is done.
 */
- (void)didFinishWiFiSpeedTestWithSampleArray:(NSMutableDictionary*)_sampleDict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the download speed test is done.
 */
- (void)didFinishDownloadSpeedTest:(NSMutableDictionary*)_sampleDict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the upload speed test is done.
 */
- (void)didFinishUploadSpeedTest:(NSMutableDictionary*)_sampleDict andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the network trace test is done.
 */
- (void)didFinishNetworkTraceTest:(NSMutableDictionary*)_sampleArray andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after the getting resource usage is done.
 */
- (void)didFinishGettingResourceUsage:(NSMutableDictionary*)_sampleArray andIsSuccess:(BOOL)isSuccess withMessage:(NSString*)_message;

/**
 * Delegate function which is called after finding the ISP Ip.
 */
- (void)didIspIPFound:(NSString*)ispIp;

/**
 * Delegate function which is called after calculating the gateway ping time.
 */
- (void)didFinishGateway:(NSMutableArray*)testResults withTotalPingTime:(float)pingTime;

/**
 * Delegate function which is called after calculating the traceroute delay test ping time.
 */
- (void)didFinishTraceRouteForDelayTest:(NSMutableArray*)testResults withTotalPingTime:(float)pingTime;

/**
 * Delegate function which is called after calculating the content test ping time.
 */
- (void)didFinishContentTest:(NSMutableArray*)testResults withTotalPingTime:(float)pingTime;

/**
 * Delegate function which is called for each sample collected for wifi router link.
 */
- (void)didPartialSampleForWifiTest:(long long)sampleValue;

/**
 * Delegate function which is called for each sample collected for download speed.
 */
- (void)didPartialSampleForDownloadTest:(float)sampleValue;

/**
 * Delegate function which is called for each sample collected for upload speed.
 */
- (void)didPartialSampleForUploadTest:(float)sampleValue;

@end

@interface vlxPublic : NSObject

@property (nonatomic) id<vlxPublicDelegate> delegate;

/**
 * Create a vlxPublic object if not created before and assign it as the shared instance of the class.
 *
 * @return shared instance of vlxPublic class.
 */
+ (vlxPublic*)sharedInstance;

/**
 * @return ssid information of currently connected wi-fi hotspot
 */
- (NSString*)getSSID;

/**
 * @return bssid information of currently connected wi-fi hotspot
 */
- (NSString*)getBSSID;

/**
 * Pings the given address _count times. Calls didFinishPingTest:andIsSuccess:withMessage: delegate method with an array which contains the ping times when the test is done.
 */
- (void)startPingTestToAddress:(NSString*)_address withCount:(int)_count;
- (void)startPingTestToAddress:(NSString*)_address withCount:(int)_count withTransactionId:(NSString*)vlxTransactionId;

/**
 * Makes a trace route to the given address. Calls didFinishTraceRoute:andIsSuccess:withMessage: delegate method with an array which contains the hops when the test is done.
 */
- (void)startTraceRouteToAddress:(NSString*)_address;
- (void)startTraceRouteToAddress:(NSString*)_address withTransactionId:(NSString*)vlxTransactionId withUploadUrl:(NSString*)uploadUrl;

/**
 * Starts a delay test to gateway, ISP and _address _count times. Calls didFinishDelayTest:andIsSuccess:withMessage: delegate method with samples array when the test is done.
 */
- (void)startDelayTestWithContentAddress:(NSString*)_address andCount:(int)_count;

/**
 * Starts a wi-fi speed test by sending udp packets to the gateway _count times. Calls didFinishWiFiSpeedTestWithSampleArray:andIsSuccess:withMessage: delegate method with samples when the test is done.
 */
- (void)startWiFiSpeedTestWithCount:(int)count;

/**
 * Starts a download test with the given download url and thread count (_tCount) by sampling _sCount times. Calls didFinishDownloadSpeedTest:andIsSuccess:withMessage: delegate method with samples when the test is done.
 */
- (void)startWithDownloadURL:(NSString*)url andSampleCount:(int)_sCount andThreadCount:(int)_tCount;
- (void)startWithDownloadURL:(NSString*)url andSampleCount:(int)_count andThreadCount:(int)_tCount withTransactionId:(NSString*)vlxTransactionId withUploadUrl:(NSString*)uploadUrl;

/**
 * Starts an upload test with the given upload url and thread count (_tCount) by sampling _count times. Calls didFinishUploadSpeedTest:andIsSuccess:withMessage: delegate method with samples when the test is done.
 */
- (void)startWithUploadURL:(NSString*)url andSampleCount:(int)_count andThreadCount:(int)_tCount;
- (void)startWithUploadURL:(NSString*)url andSampleCount:(int)_count andThreadCount:(int)_tCount withTransactionId:(NSString*)vlxTransactionId withUploadUrl:(NSString*)uploadUrl;
;

/**
 * Starts network trace test
 */
- (void)startNetworkTraceTest;

/**
 * Starts getting resource usage
 */
- (void)startGettingResourceUsage;

/**
 * Stop the wifi test if it's running.
 */
- (void)stopWifiTest;

- (void)stopDownloadTest;

- (void)stopUploadTest;

- (void)startScanNetwork;

@end
