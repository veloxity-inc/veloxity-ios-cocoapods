Pod::Spec.new do |s|
  s.name             = "VeloxitySDK"
  s.module_name      = "VeloxitySDK"
  s.version          = "3.9.28"
  s.summary          = "Veloxity iOS SDK"
  s.description      = "Veloxity SDK For IOS\n"
  s.homepage         = "https://bitbucket.org/veloxity-inc/veloxity-ios-cocoapods"
  s.license          = "Proprietary License"
  s.author           = { "veloxity" => "developer@veloxity.net" }
  s.source           = { :git => "https://bitbucket.org/veloxity-inc/veloxity-ios-cocoapods", :tag => s.version.to_s }

  s.platform     = :ios, '12.0'
  s.requires_arc = true

  s.source_files = "Pod/VeloxitySDK.framework/Headers/*.h"
  s.public_header_files = "Pod/VeloxitySDK.framework/Headers/*.h"
  s.preserve_paths = "Pod/VeloxitySDK.framework"
  s.vendored_frameworks = "Pod/VeloxitySDK.framework"
  s.frameworks = "Foundation", "UIKit", "CFNetwork", "CoreLocation", "SystemConfiguration", "CoreTelephony", "Security", "VeloxitySDK"
  s.libraries = "z", "sqlite3"
  s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libz" }
  s.pod_target_xcconfig = {
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end
