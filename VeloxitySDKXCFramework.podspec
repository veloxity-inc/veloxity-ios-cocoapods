Pod::Spec.new do |s|
  s.name             = "VeloxitySDKXCFramework"
  s.module_name      = "VeloxitySDKXCFramework"
  s.version          = "4.0.9"
  s.summary          = "Veloxity iOS SDK"
  s.description      = "Veloxity SDK For IOS(XCFramework)\n"
  s.homepage         = "https://bitbucket.org/veloxity-inc/veloxity-ios-cocoapods"
  s.license          = "Proprietary License"
  s.author           = { "veloxity" => "developer@veloxity.net" }
  s.source           = { :git => "https://bitbucket.org/veloxity-inc/veloxity-ios-cocoapods", :tag => s.version.to_s }

  s.platform     = :ios, '12.0'
  s.requires_arc = true

  s.ios.vendored_frameworks = "Pod/VeloxitySDK.xcframework"
  s.preserve_paths = "Pod/VeloxitySDK.xcframework"
  
  s.frameworks = "Foundation", "UIKit", "CFNetwork", "CoreLocation", "SystemConfiguration", "CoreTelephony", "Security", "VeloxitySDK"
  s.libraries = "z", "sqlite3"
  s.pod_target_xcconfig = {
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }


end
