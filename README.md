# VeloxitySDK

[![Version](https://img.shields.io/cocoapods/v/VeloxitySDK.svg?style=flat)](http://cocoapods.org/pods/VeloxitySDK)
[![License](https://img.shields.io/cocoapods/l/VeloxitySDK.svg?style=flat)](http://cocoapods.org/pods/VeloxitySDK)
[![Platform](https://img.shields.io/cocoapods/p/VeloxitySDK.svg?style=flat)](http://cocoapods.org/pods/VeloxitySDK)

# About
Veloxity is the segmentation, engagement & targeting notification suite engineered for the mobile era.

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
iOS 8+

Xcode 8

## Installation

VeloxitySDK is available through [CocoaPods](http://cocoapods.org). To install it, simply add the following line to your Podfile:

```ruby
pod "VeloxitySDK"
```

## Importing the framework

If you're in swift:

```swift
import "VeloxitySDK"
```

If you're in Objective-C:

```objective-c
#import <VeloxitySDK/Veloxity.h>
```

## Author

Veloxity Inc., contact us: developer@veloxity.net
